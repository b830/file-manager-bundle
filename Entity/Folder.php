<?php

namespace App\Akip\FileManagerBundle\Entity;

use App\Akip\EshopBundle\Entity\ErrorMessages;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use phpDocumentor\Reflection\Types\Collection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Folder
 *
 * @ORM\Table(name="folder", indexes={@ORM\Index(name="fk_main_id", columns={"main_id"}), @ORM\Index(name="parent_id", columns={"parent_id"})})
 * @ORM\Entity(repositoryClass="App\Akip\FileManagerBundle\Repository\FolderRepository")
 */
class Folder
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Groups({"list"})
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     * @Groups({"list"})
     */
    private $name;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="slug", type="string", length=255, nullable=false)
     * @Groups({"list"})
     */
    private $slug;

    /**
     * @var int
     *
     * @ORM\Column(name="lft", type="integer", nullable=false)
     */
    private $lft = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="rgt", type="integer", nullable=false)
     */
    private $rgt = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="depth", type="integer", nullable=false)
     */
    private $depth = 0;

    /**
     * @var Folder
     *
     * @ORM\ManyToOne(targetEntity="Folder")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="main_id", referencedColumnName="id")
     * })
     */
    private $main;

    /**
     * @var Folder
     *
     * @ORM\ManyToOne(targetEntity="Folder")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="parent_id", referencedColumnName="id")
     * })
     * @Groups({"list"})
     */
    private $parent;

    /**
     * @var Collection|File[]
     * @ORM\OneToMany(targetEntity="App\Akip\FileManagerBundle\Entity\File", mappedBy="folder")
     */
    private $files;

    public function __construct()
    {
        $this->files = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        if ($name === '' || !$name){
            ErrorMessages::message(ErrorMessages::CANNOT_BE_EMPTY, 'Name ');
        }
        $this->name = $name;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        if ($slug === '' || !$slug){
            ErrorMessages::message(ErrorMessages::CANNOT_BE_EMPTY, 'Slug ');
        }
        $this->slug = $slug;

        return $this;
    }

    public function getLft(): ?int
    {
        return $this->lft;
    }

    public function setLft(int $lft): self
    {
        $this->lft = $lft;

        return $this;
    }

    public function getRgt(): ?int
    {
        return $this->rgt;
    }

    public function setRgt(int $rgt): self
    {
        $this->rgt = $rgt;

        return $this;
    }

    public function getDepth(): ?int
    {
        return $this->depth;
    }

    public function setDepth(int $depth): self
    {
        $this->depth = $depth;

        return $this;
    }

    public function getMain(): ?self
    {
        return $this->main;
    }

    public function setMain(?self $main): self
    {
        $this->main = $main;

        return $this;
    }

    public function getParent(): ?self
    {
        return $this->parent;
    }

    public function setParent(?self $parent): self
    {
        $this->parent = $parent;

        return $this;
    }

    public function load($data, Folder $mainFolder, Folder $parent = null)
    {
        $this->setName($data['name']);
        $this->setSlug($data['slug']);
        $this->setMain($mainFolder);
        $this->setParent($parent);
        return $this;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection|File[]
     */
    public function getFiles(): \Doctrine\Common\Collections\Collection
    {
        return $this->files;
    }

    public function getGroupedFiles()
    {
        return $this->files->filter(function (File $file) {
            if (!$file->getChildren()->isEmpty() or (int)$file->getSize() === 0) {
                if ($file->getSize() !== '0')
                    return $file;
            }
        });
    }

    public function addFile(File $file): self
    {
        if (!$this->files->contains($file)) {
            $this->files[] = $file;
            $file->setFolder($this);
        }

        return $this;
    }

    public function removeFile(File $file): self
    {
        if ($this->files->contains($file)) {
            $this->files->removeElement($file);
            // set the owning side to null (unless already changed)
            if ($file->getFolder() === $this) {
                $file->setFolder(null);
            }
        }

        return $this;
    }

}
