<?php

namespace App\Akip\FileManagerBundle\Entity;

use App\Akip\EshopBundle\Entity\ErrorMessages;
use App\Akip\EshopBundle\Entity\Product;
use App\Akip\EshopBundle\Entity\ProductPhoto;
use App\Akip\EshopBundle\Entity\ProductVariant;
use App\Akip\EshopBundle\Entity\ProductVariantPhoto;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * File
 *
 * @ORM\Table(name="file", indexes={@ORM\Index(name="folder_item_id", columns={"folder_item_id"})})
 * @ORM\Entity(repositoryClass="App\Akip\FileManagerBundle\Repository\FileRepository")
 */
class File
{
    /**
     * @var string
     *
     * @ORM\Column(name="uuid", type="guid", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="UUID")
     * @Groups({"detail", "list"})
     */
    private $uuid;

    /**
     * @var string
     *
     * @ORM\Column(name="original_name", type="string", length=255, nullable=false)
     * @Groups({"detail", "list"})
     */
    private $originalName;

    /**
     * @var string
     *
     * @ORM\Column(name="original_suffix", type="string", length=10, nullable=false)
     * @Groups({"detail", "list"})
     */
    private $originalSuffix;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     * @Groups({"detail", "list"})
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=10, nullable=false)
     * @Groups({"detail", "list"})
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="size", type="string", length=20, nullable=false)
     * @Groups({"detail", "list"})
     */
    private $size = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="suffix", type="string", length=10, nullable=false)
     * @Groups({"detail", "list"})
     */
    private $suffix;

    /**
     * @var Folder
     *
     * @ORM\ManyToOne(targetEntity="Folder")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="folder_item_id", referencedColumnName="id")
     * })
     * @Groups({"detail"})
     */
    private $folder;

    /**
     * @var Collection|File[]
     * @ORM\OneToMany(targetEntity="App\Akip\FileManagerBundle\Entity\File", mappedBy="parentFile")
     * @Groups({"detail", "list"})
     */
    private $children;

    /**
     * @var File
     * @ORM\ManyToOne(targetEntity="App\Akip\FileManagerBundle\Entity\File", inversedBy="children")
     *
     * @ORM\JoinColumn(name="parent_file_uuid", referencedColumnName="uuid")
     *
     */
    private $parentFile;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\OneToMany(targetEntity="App\Akip\EshopBundle\Entity\ProductPhoto", mappedBy="photo")
     */
    private $productPhoto;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\OneToMany(targetEntity="App\Akip\EshopBundle\Entity\ProductVariantPhoto", mappedBy="photo")
     */
    private $productVariantPhoto;

    public function __construct()
    {
        $this->children = new ArrayCollection();
        $this->productInspiration = new ArrayCollection();
        $this->productVariantInspiration = new ArrayCollection();
        $this->productPhoto = new ArrayCollection();
        $this->productVariantPhoto = new ArrayCollection();
    }

    public function getUuid(): ?string
    {
        return $this->uuid;
    }

    public function getOriginalName(): ?string
    {
        return $this->originalName;
    }

    public function setOriginalName(string $originalName): self
    {
        if ($originalName === '' || !$originalName){
            ErrorMessages::message(ErrorMessages::CANNOT_BE_EMPTY, 'Original name ');
        }
        $this->originalName = $originalName;

        return $this;
    }

    public function getOriginalSuffix(): ?string
    {
        return $this->originalSuffix;
    }

    public function setOriginalSuffix(string $originalSuffix): self
    {
        if ($originalSuffix === '' || !$originalSuffix){
            ErrorMessages::message(ErrorMessages::CANNOT_BE_EMPTY, 'Original suffix ');
        }
        $this->originalSuffix = $originalSuffix;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        if ($type === '' || !$type){
            ErrorMessages::message(ErrorMessages::CANNOT_BE_EMPTY, 'Type ');
        }
        $this->type = $type;

        return $this;
    }

    public function getSize(): ?string
    {
        return $this->size;
    }

    public function setSize(string $size): self
    {
        $this->size = $size;

        return $this;
    }

    public function getSuffix(): ?string
    {
        return $this->suffix;
    }

    public function setSuffix(string $suffix): self
    {
        if ($suffix === '' || !$suffix){
            ErrorMessages::message(ErrorMessages::CANNOT_BE_EMPTY, 'Suffix ');
        }
        $this->suffix = $suffix;

        return $this;
    }

    public function getFolder(): ?Folder
    {
        return $this->folder;
    }

    public function setFolder(?Folder $folder): self
    {
        $this->folder = $folder;

        return $this;
    }

    public function load($data, $type, Folder $folder, File $parentFile = null)
    {
        $fileInfo = pathinfo($data['originalName']);
        $this->setFolder($folder);
        $this->setOriginalName($fileInfo['filename']);
        $this->setOriginalSuffix($fileInfo['extension']);
        $this->setType($type);
        $this->setParentFile($parentFile);
        if (isset($data['name'])) {
            $this->setName($data['name']);
        } else {
            $this->setName($fileInfo['filename']);
        }
        if (isset($data['suffix'])) {
            $this->setSuffix($data['suffix']);
        } else {
            $this->setSuffix($fileInfo['extension']);
        }
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        if ($name === '' || !$name){
            ErrorMessages::message(ErrorMessages::CANNOT_BE_EMPTY, 'Name ');
        }
        $this->name = $name;

        return $this;
    }

    public function getParentFile(): ?self
    {
        return $this->parentFile;
    }

    public function setParentFile(?self $parentFile): self
    {
        $this->parentFile = $parentFile;

        return $this;
    }

    /**
     * @return Collection|File[]
     */
    public function getChildren(): Collection
    {
        return $this->children;
    }

    public function addChild(File $child): self
    {
        if (!$this->children->contains($child)) {
            $this->children[] = $child;
            $child->setParentFile($this);
        }

        return $this;
    }

    public function removeChild(File $child): self
    {
        if ($this->children->contains($child)) {
            $this->children->removeElement($child);
            // set the owning side to null (unless already changed)
            if ($child->getParentFile() === $this) {
                $child->setParentFile(null);
            }
        }

        return $this;
    }

    /**
     * @return string
     * @Groups({"detail", "list"})
     */
    public function getUrl()
    {
        if ($this->getParentFile()) {
            return $_ENV['PROTOCOL'].'://'. $_ENV['HOST_NAME']  . $this->createImgPath($this->getParentFile()->uuid, $this->uuid);
        } else {
            return $_ENV['PROTOCOL'].'://'. $_ENV['HOST_NAME']  . $this->createImgPath($this->uuid);
        }
    }

    /**
     * @return Collection|ProductPhoto[]
     */
    public function getProductPhoto(): Collection
    {
        return $this->productPhoto;
    }

    public function addProductPhoto(ProductPhoto $productPhoto): self
    {
        if (!$this->productPhoto->contains($productPhoto)) {
            $this->productPhoto[] = $productPhoto;
            $productPhoto->setPhoto($this);
        }

        return $this;
    }

    public function removeProductPhoto(ProductPhoto $productPhoto): self
    {
        if ($this->productPhoto->contains($productPhoto)) {
            $this->productPhoto->removeElement($productPhoto);
            // set the owning side to null (unless already changed)
            if ($productPhoto->getPhoto() === $this) {
                $productPhoto->setPhoto(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ProductVariantPhoto[]
     */
    public function getProductVariantPhoto(): Collection
    {
        return $this->productVariantPhoto;
    }

    public function addProductVariantPhoto(ProductVariantPhoto $productVariantPhoto): self
    {
        if (!$this->productVariantPhoto->contains($productVariantPhoto)) {
            $this->productVariantPhoto[] = $productVariantPhoto;
            $productVariantPhoto->setPhoto($this);
        }

        return $this;
    }

    public function removeProductVariantPhoto(ProductVariantPhoto $productVariantPhoto): self
    {
        if ($this->productVariantPhoto->contains($productVariantPhoto)) {
            $this->productVariantPhoto->removeElement($productVariantPhoto);
            // set the owning side to null (unless already changed)
            if ($productVariantPhoto->getPhoto() === $this) {
                $productVariantPhoto->setPhoto(null);
            }
        }

        return $this;
    }
    public function checkFolderName(string $folderName, string $fileName): bool
    {
        if (substr($folderName, 0, 3) === substr($fileName, 0, 3)) {
            return true;
        } else {
            return false;
        }
    }

    public function createImgPath($parentUuid, $uuid = null)
    {
        $path = '';
        $fullPath = \dirname(__DIR__);
        $delimitedPath = explode('/', $fullPath);
        foreach ($delimitedPath as $item) {
            $path .= $item . '/';
            if ($item === 'www') {
                $path = rtrim($path, '/');
                break;
            }
        }
        $path .= '/public/files';
        if ($parentUuid === null) {
            return '/img/File_not_found.png';
        }
        if ($uuid == null) {
            $uuid = $parentUuid;
        }
        if (!file_exists($path)) {
            return '/img/File_not_found.png';
        }
        $filePath = glob("{$path}/{$parentUuid}/{$uuid}.[!w]*");
        if (!empty($filePath)) {
            return '/files' . str_replace($path,'',$filePath[0]);
        } else {
            $finder = new Finder();
            foreach ($finder->directories()->in($path) as $item) {
                if ($this->checkFolderName($item->getFilename(), $parentUuid)) {
                    $filePath = glob("{$item->getFilename()}/{$uuid}.[!w]*");
                }
                if (!empty($filePath)) {
                    return '/files' . str_replace($path,'',$filePath[0]);
                } else {
                    return '/img/File_not_found.png';
                }
            }
        }
    }

}
