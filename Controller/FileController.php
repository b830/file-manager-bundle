<?php

namespace App\Akip\FileManagerBundle\Controller;

use App\Akip\CmsBundle\Entity\ErrorMessages;
use App\Akip\EshopBundle\Entity\Category;
use App\Akip\EshopBundle\Entity\ProductInspiration;
use App\Akip\EshopBundle\Entity\ProductVariantInspiration;
use App\Akip\FileManagerBundle\Entity\File;
use App\Akip\FileManagerBundle\Entity\Folder;
use App\Akip\FileManagerBundle\Entity\FolderItem;
use App\Akip\FileManagerBundle\Repository\FileRepository;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\FOSRestBundle;
use FOS\RestBundle\Request\ParamFetcherInterface;
use MongoDB\BSON\Regex;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Normalizer\DataUriNormalizer;
use function Symfony\Component\String\u;

/**
 * Class FileController
 *
 * @package App\Akip\FileManagerBundle\Controller
 * @Rest\Route("/api/file", name="file_")
 */
class FileController extends BaseController
{
    /**
     * @var EntityManagerInterface
     */
    private $em;
    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * FileController constructor.
     *
     * @param EntityManagerInterface $em
     * @param Filesystem             $filesystem
     */
    public function __construct(EntityManagerInterface $em, Filesystem $filesystem)
    {
        $this->em = $em;
        $this->filesystem = $filesystem;

    }

    /**
     * @Rest\Post("", name="create")
     * @Rest\View(serializerGroups={"detail"})
     * @param Request $request
     */
    public function save(Request $request)
    {
        $path = $this->getParameter('kernel.project_dir') . '/public/files';
        if (!file_exists($path)) {
            $this->filesystem->mkdir($path);
        }
        $data = json_decode($request->getContent(), true);
        if (isset($data['watermark'])) {
            $waterMark = $data['watermark'];
        } else {
            $waterMark = true;
        }
        if (empty($data) || !$data) {
            throw new HttpException(Response::HTTP_UNPROCESSABLE_ENTITY, 'Empty body');
        }
//        return $_ENV;
        $file = new File();
        $folder = $this->getDoctrine()->getRepository(Folder::class)->find($data['folder']);

        if (!$folder)
            throw new HttpException(Response::HTTP_NOT_FOUND, 'Folder with specified id not found');

        //najdeme MIME type
        preg_match('#[a-z]+/[a-z]+#', $data['data'], $type);

        $base64 = preg_replace('#data:[a-z]+/[^;]+;base64,#', '', $data['data']);
        $fileData = base64_decode($base64);

        $file->load($data, $type[0], $folder);

        $this->em->persist($file);
        $this->em->flush();

        $finder = new Finder();
        //existuju li nejake slozky v public/files
        if (($finder->directories()->in($path))->count() === 0) {
            $folderPath = "{$path}/{$file->getUuid()}";
            $this->filesystem->mkdir($folderPath);
        } else {
            foreach ($finder->directories()->in($path) as $item) {
                if ($this->checkFolderName($item->getFilename(), $file->getUuid())) {
                    $folderPath = "{$path}/{$item->getFilename()}";
                } else {
                    $folderPath = "{$path}/{$file->getUuid()}";
                    $this->filesystem->mkdir($folderPath);
                }
            }
        }
        if (preg_match('#^image/[a-z]+#', $file->getType())) {

            file_put_contents("{$folderPath}/{$file->getUuid()}.{$file->getSuffix()}", $this->saveImg($file, $fileData, getimagesizefromstring($fileData)[0], getimagesizefromstring($fileData)[1], 'original', $waterMark));
            $this->convertImageToWebP("{$folderPath}/{$file->getUuid()}.{$file->getSuffix()}", "{$folderPath}/{$file->getUuid()}.webp");
            $defaultImg = new File();
            $defaultImg->load($data, $file->getType(), $file->getFolder(), $file);

            $file->addChild($defaultImg);
            $this->em->persist($defaultImg);
            $this->em->flush();


            $defaultSize = $this->getDefaultSize();
            $newSize = $this->calculateNewSize(getimagesizefromstring($fileData)[0], getimagesizefromstring($fileData)[1], $defaultSize['width'], $defaultSize['height']);
            file_put_contents("{$folderPath}/{$defaultImg->getUuid()}.{$file->getSuffix()}", $this->saveImg($defaultImg, $fileData, $newSize[0], $newSize[1], 'default', $waterMark));
            $this->convertImageToWebP("{$folderPath}/{$defaultImg->getUuid()}.{$file->getSuffix()}", "{$folderPath}/{$defaultImg->getUuid()}.webp");

            foreach ($this->getConfigSizes() as $size) {
                $newImg = new File();
                $newImg->load($data, $file->getType(), $file->getFolder(), $file);
                $file->addChild($newImg);
                $this->em->persist($newImg);
                $this->em->flush();

                $newSize = $this->calculateNewSize(getimagesizefromstring($fileData)[0], getimagesizefromstring($fileData)[1], $size['width'], $size['height']);
                file_put_contents("{$folderPath}/{$newImg->getUuid()}.{$file->getSuffix()}", $this->saveImg($newImg, $fileData, $newSize[0], $newSize[1], "{$newSize[0]}x{$newSize[1]}", $waterMark));
                $this->convertImageToWebP("{$folderPath}/{$newImg->getUuid()}.{$file->getSuffix()}", "{$folderPath}/{$newImg->getUuid()}.webp");
            }
            return $this->getFileData($file);
        } else {
            file_put_contents("{$folderPath}/{$file->getUuid()}.{$file->getSuffix()}", $fileData);
        }

        return $file;
    }

    private function calculateNewSize($oldWidth, $oldHeight, $newWidth, $newHeight)
    {
        if ($oldWidth > $oldHeight) {
            $scale = $oldWidth / $newWidth;
            $height = $oldHeight / $scale;
            return [$newWidth, (int)$height];
        } elseif ($oldHeight > $oldWidth) {
            $scale = $oldHeight / $newHeight;
            $width = $oldWidth / $scale;
            return [$width, (int)$newHeight];
        } elseif ($oldWidth === $oldHeight) {
            return [$newWidth, $newHeight];
        }
    }

    private function saveImg(File $file, $data, $newWidth, $newHeight, $name = '', $waterMark = true)
    {
        //original width x height
        $imgInfo = getimagesizefromstring($data);

        $image = imagecreatefromstring($data);

        if ($waterMark && $name !== 'original') {
            $this->addWatermark($image);
        }
        //Resample
        $image_p = imagecreatetruecolor($newWidth, $newHeight);
        if ($file->getType() === "image/png" || $file->getType() === "image/gif") {
            imagecolortransparent($image_p, imagecolorallocatealpha($image_p, 0, 0, 0, 127));
            imagealphablending($image_p, false);
            imagesavealpha($image_p, true);
        }
        imagecopyresampled($image_p, $image, 0, 0, 0, 0, $newWidth, $newHeight, $imgInfo[0], $imgInfo[1]);

        //Buffering; https://stackoverflow.com/questions/12232250/resizing-base64-images
        ob_start();
        imagepng($image_p);
        $result = ob_get_contents();
        ob_end_clean();
        $file->setSize("{$newWidth}x{$newHeight}");
        $file->setName("{$file->getName()}_{$name}");
        $this->em->flush();
        return $result;
    }

    private function addWatermark($image)
    {
        $text = $_ENV['WATERMARK_TEXT'];
        $fontsize = 45;
        $angle = 30;
        $font = $this->getParameter('kernel.project_dir') . '/public/fonts/arial-mt-extra-bold.ttf';
        $color = imagecolorallocatealpha($image, 255, 255, 255, 70); //white
        // Add text to image
        $bbox = imagettfbbox($fontsize, 0, $font, $text);
        $tbwidth = abs($bbox[2]);
        $factor = round((imagesx($image) / $tbwidth), 0, PHP_ROUND_HALF_DOWN); //ie. 800/240=3
        if (imagesy($image) > imagesx($image)) {
            $newfontsize = $fontsize * $factor; //ie. 12(pt) * 3 = 36
        } else {
            $newfontsize = $fontsize * ($factor / 2); //ie. 12(pt) * 3 = 36
        }
        list($x, $y) = $this->ImageTTFCenter($image, $text, $font, $newfontsize, $angle);
        imagettftext($image, $newfontsize, $angle, $x, $y, $color, $font, $text);
    }

    private function ImageTTFCenter($image, $text, $font, $size, $angle = 45)
    {
        $xi = imagesx($image);
        $yi = imagesy($image);

        $box = imagettfbbox($size, $angle, $font, $text);

        $xr = abs(max($box[2], $box[4]));
//        $yr = abs(max($box[1], $box[5]));

        $x = intval(($xi - $xr) / 2);
        $y = intval(($yi + ($box[1] - $box[5])) / 2);

        return array($x, $y);
    }

    private function filePath($file, $path)
    {
        if ($file->getParentFile() === null) {
            $filePath = "{$path}/{$file->getUuid()}/{$file->getUuid()}.{$file->getSuffix()}";
        }
        if (!file_exists($filePath)) {
            $finder = new Finder();
            foreach ($finder->directories()->in($path) as $item) {
                if ($this->checkFolderName($item->getFilename(), $file->getUuid())) {
                    $filePath = "{$path}/{$item->getFilename()}/{$file->getUuid()}.{$file->getSuffix()}";
                }

            }
        }
        return $filePath;
    }

    /**
     * @Rest\Get("/{uuid}", name="get_data")
     *
     * @param File $file
     */
    public function getFile(File $file = null)
    {
        if (!$file) {
            throw new HttpException(Response::HTTP_NOT_FOUND, 'File with specified id not found');
        }
        $path = $this->getParameter('kernel.project_dir') . '/public/files';
        if (!file_exists($path)) {
            $this->filesystem->mkdir($path);
        }
        if ($file->getParentFile() === null) {
            $filePath = "{$path}/{$file->getUuid()}/{$file->getUuid()}.{$file->getSuffix()}";
        } else {
            $filePath = "{$path}/{$file->getParentFile()->getUuid()}/{$file->getUuid()}.{$file->getSuffix()}";
        }
        if (!file_exists($filePath)) {
            $finder = new Finder();
            foreach ($finder->directories()->in($path) as $item) {
                if ($this->checkFolderName($item->getFilename(), $file->getUuid())) {
                    $filePath = "{$path}/{$item->getFilename()}/{$file->getUuid()}.{$file->getSuffix()}";
                }
            }
            if (!file_exists($filePath)) {
                $response = new Response();
                $disposition = $response->headers->makeDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, "File_not_found.{$file->getOriginalSuffix()}");

                $response->headers->set('Content-Disposition', $disposition);
                $response->headers->set('Content-Type', $file->getType());
                $response->setContent(file_get_contents('https://i.redd.it/s8lk86v3r2m11.png'));

                return $response;
            }
        }

        $response = new Response();
        $name = u($file->getOriginalName())->ascii();
        $disposition = $response->headers->makeDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, "{$name}.{$file->getOriginalSuffix()}");

        $response->headers->set('Content-Disposition', $disposition);
        $response->headers->set('Content-Type', $file->getType());
        $response->setContent(file_get_contents($filePath));

        return $response;
    }

    /**
     * @Rest\Delete("/{id}", name="delete")
     *
     * @param File $file
     */
    public function delete(File $file = null, $deleteFolder = true)
    {
        if (!$file) {
            throw new HttpException(Response::HTTP_NOT_FOUND, 'File with specified id not found');
        }

        $path = $this->getParameter('kernel.project_dir') . '/public/files';
        if (!file_exists($path)) {
            $this->filesystem->mkdir($path);
        }
        if ($file->getParentFile() === null) {
            $filePath = "{$path}/{$file->getUuid()}/{$file->getUuid()}.{$file->getSuffix()}";
            $filePathWebp = "{$path}/{$file->getUuid()}/{$file->getUuid()}.webp}";
        } else {
            $filePath = "{$path}/{$file->getParentFile()->getUuid()}/{$file->getUuid()}.{$file->getSuffix()}";
            $filePathWebp = "{$path}/{$file->getParentFile()->getUuid()}/{$file->getUuid()}.webp}";
        }
        $finder = new Finder();
        if (!file_exists($filePath)) {
            foreach ($finder->directories()->in($path) as $item) {
                if ($this->checkFolderName($item->getFilename(), $file->getUuid())) {
                    $filePath = "{$path}/{$item->getFilename()}/{$file->getUuid()}.{$file->getSuffix()}";
                    $filePathWebp = "{$path}/{$item->getFilename()}/{$file->getUuid()}.webp}";
                }
            }
        }
        $folderPath = substr(explode('.', $filePath)[0], 0, strlen(explode('.', $filePath)[0]) - 36);

        if (file_exists($folderPath)) {
            if (file_exists($filePath)) {
                $this->filesystem->remove($filePath);
                $this->filesystem->remove($filePathWebp);
            }
            if ($finder->files()->in($folderPath) && $deleteFolder) {
                $this->filesystem->remove($folderPath);
            }
        }
        foreach ($file->getProductVariantPhoto() as $item) {
            $this->em->remove($item);
        }
        foreach ($file->getProductPhoto() as $item) {
            if ($item->getTranslationsObj()) {
                foreach ($item->getTranslationsObj() as $translation) {
                    $this->em->remove($translation);
                }
            }
            $this->em->remove($item);
        }
        $productInspirations = $this->em->getRepository(ProductInspiration::class)->findBy(['file' => $file->getUuid()]);
        foreach ($productInspirations as $item) {
            if ($item->getTranslationsObj()) {
                foreach ($item->getTranslationsObj() as $translation) {
                    $this->em->remove($translation);
                }
            }
            $this->em->remove($item);
        }
        $productVariantInspirations = $this->em->getRepository(ProductVariantInspiration::class)->findBy(['file' => $file->getUuid()]);
        foreach ($productVariantInspirations as $item) {
            $this->em->remove($item);
        }
        $categories = $this->em->getRepository(Category::class)->findBy(['icon' => $file]);
        foreach ($categories as $category) {
            $category->setIcon(null);
        }
        foreach ($file->getChildren() as $item) {
            $file->removeChild($item);
            $this->em->remove($item);
        }
        $this->em->flush();
        $this->em->remove($file);
        $this->em->flush();
    }

    /**
     * @Rest\Get("/data/{uuid}", name="get")
     * @Rest\View(serializerGroups={"detail"})
     *
     * @param File $file
     *
     * @return File
     */
    public function getFileData(File $file = null)
    {
        if (!$file) {
            throw new HttpException(Response::HTTP_NOT_FOUND, 'File with specified id not found');
        }
        return $file;
    }

    /**
     * @Rest\Put("/update_folder", name="update_folder")
     * @Rest\View(serializerGroups={"detail"})
     * @param Request $request
     */
    public function updateFolder(Request $request)
    {
        $data = json_decode($request->getContent(), true);
        if (empty($data) || !$data) {
            ErrorMessages::message(ErrorMessages::EMPTY_BODY);
        }
        $validData = [];
        foreach ($data as $item) {
            /** @var $file File */
            $file = $this->em->getRepository(File::class)->findOneBy(['uuid' => $item['uuid']]);
            if (!$file) {
                ErrorMessages::message(ErrorMessages::FILE_NOT_FOUND);
            }
            if (isset($item['folderId']) && $item['folderId']) {
                /** @var Folder $folder */
                $folder = $this->em->getRepository(Folder::class)->find($item['folderId']);
                if (!$folder) {
                    ErrorMessages::message(ErrorMessages::FOLDER_NOT_FOUND);
                }
                $file->setFolder($folder);
                $validData[] = $file;
            }
        }
        if (count($validData) > 0) {
            foreach ($validData as $item) {
                $this->em->persist($item);
            }
            $this->em->flush();
            return $validData;
        }
    }

    /**
     * @Rest\Put("/{uuid}", name="update")
     * @Rest\View(serializerGroups={"detail"})
     *
     * @param File|null $file
     */
    public function update(Request $request, File $file = null)
    {
        if (!$file) {
            throw new HttpException(Response::HTTP_NOT_FOUND, 'File with specified id not found');
        }
        $data = json_decode($request->getContent(), true);
        if (empty($data) || !$data) {
            throw new HttpException(Response::HTTP_UNPROCESSABLE_ENTITY, 'Empty body');
        }

    }

    /**
     * @Rest\Patch("/towebp", name="patch_webp")
     * @Rest\View(serializerGroups={"detail"})
     *
     */
    public function patchToWebp(Request $request)
    {
        set_time_limit(600);
        $path = [$this->getParameter('kernel.project_dir') . '/public/files'];
        $finder = new Finder();
        foreach ($path as $item) {
            $this->loopAndConvert($item, $finder);
        }

    }

    private function loopAndConvert($path, Finder $finder)
    {
        if ($finder->files()->in($path)->count() > 0) {
            foreach ($finder->files()->in($path) as $item) {
                $this->convertImageToWebP($item->getRealPath(), $item->getPath() . '/' . $item->getFilenameWithoutExtension() . '.webp');
            }
        }
    }

    private function convertImageToWebP($source, $destination, $quality = 80)
    {
        //https://stackoverflow.com/questions/42893841/in-php-why-has-imagecreatefromjpeg-not-a-jpeg-file-changed-to-a-fatal-error
        $mime = mime_content_type($source);
        $image = false;
        if ($mime == 'image/jpeg' || $mime == 'image/jpg')
            $image = imagecreatefromjpeg($source);
        elseif ($mime == 'image/gif')
            $image = imagecreatefromgif($source);
        elseif ($mime == 'image/png') {
            $image = imagecreatefrompng($source);
            imagepalettetotruecolor($image);
        }
        if ($image) {

            imagewebp($image, $destination, $quality);
            // Free up memory
            imagedestroy($image);
        }
    }
}
