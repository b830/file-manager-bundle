<?php

namespace App\Akip\FileManagerBundle\Controller;

use App\Akip\EshopBundle\Entity\ErrorMessages;
use App\Akip\FileManagerBundle\Entity\Folder;
use App\Akip\FileManagerBundle\Repository\FileRepository;
use App\Akip\FileManagerBundle\Repository\FolderRepository;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class FolderController
 * @package App\Akip\FileManagerBundle\Controller
 * @Rest\Route("/api/folder", name="folder_")
 */
class FolderController extends BaseController
{

    /**
     * @var EntityManagerInterface
     */
    private $em;
    /**
     * @var ValidatorInterface
     */
    private $validator;

    public function __construct(EntityManagerInterface $em, ValidatorInterface $validator)
    {
        $this->em = $em;
        $this->validator = $validator;
    }

    /**
     * @Rest\Get("", name="list")
     * @Rest\View(serializerGroups={"list"})
     * @param FolderRepository $repository
     * @return mixed
     */
    public function list(FolderRepository $repository)
    {
        $folders = $repository->findBy(['parent' => null], ['name' => 'ASC']);
        if (!$folders) {
            return [];
        }

        foreach ($folders as $folder) {
            $data[] = $repository->build($folder, $this->em, $repository->getMaxDpth($folder->getMain()));
        }
        return $data;
    }

    /**
     * @Rest\Post("", name="save")
     * @Rest\View(serializerGroups={"list"})
     */
    public function save(Request $request, FolderRepository $repository)
    {
        $data = json_decode($request->getContent(), true);
        if (empty($data) || !$data) {
            throw new HttpException(Response::HTTP_UNPROCESSABLE_ENTITY, 'Empty body');
        }
        $folder = new Folder();
        $folder->load($data, $folder);
        

        $valid = BaseController::validate($folder, $this->validator);
        if (!empty($valid))
            return $valid;

        $this->em->persist($folder);
        $this->em->flush();
        $repository->rebuildTree($folder->getMain());
        return $folder;
    }

    /**
     * @Rest\Post("/{id}", name="add_subfolder")
     * @Rest\View(serializerGroups={"list"})
     * @param Request $request
     * @param Folder $folder
     */
    public function addSubfolder(Request $request, Folder $folder, FolderRepository $repository)
    {
        if (!$folder)
            throw new HttpException(Response::HTTP_NOT_FOUND, 'Folder with specified id not found');
        $data = json_decode($request->getContent(), true);
        if (empty($data) || !$data) {
            throw new HttpException(Response::HTTP_UNPROCESSABLE_ENTITY, 'Empty body');
        }
        $newFolder = new Folder();
        $newFolder->load($data, $folder->getMain(), $folder);

        $valid = BaseController::validate($newFolder, $this->validator);
        if (!empty($valid))
            return $valid;

        $this->em->persist($newFolder);
        $this->em->flush();
        $repository->rebuildTree($folder->getMain());

        return $newFolder;
    }


    /**
     * @Rest\Put("/id", name="update")
     * @Rest\View(serializerGroups={"detail"})
     *
     * @param Request $request
     * @param FolderRepository $repository
     * @param Folder|null $folder
     */
    public function update(Request $request, FolderRepository $repository, Folder $folder = null)
    {
        if (!$folder) {
            throw new HttpException(Response::HTTP_NOT_FOUND, 'Folder with specified id not found');
        }
        $data = json_decode($request->getContent(), true);
        if (empty($data) || !$data) {
            throw new HttpException(Response::HTTP_UNPROCESSABLE_ENTITY, 'Empty body');
        }
        if (isset($data['parentId'])) {
            if ($data['parentId'] == null) {
                $parentFolder = null;
            } else {
                $parentFolder = $repository->find($data['parentId']);
                if (!$parentFolder) {
                    throw new HttpException(Response::HTTP_NOT_FOUND, 'Folder with specified id not found');
                }
            }
        } else {
            $parentFolder = $folder->getParent();
        }
        $folder->load($data, $folder->getMain(), $parentFolder);


        $valid = BaseController::validate($folder, $this->validator);
        if (!empty($valid))
            return $valid;

        $this->em->persist($folder);
        $this->em->flush();
        $repository->rebuildTree($folder->getMain());

        return $folder;
    }

    /**
     * @Rest\Delete("/{id}", name="delete")
     * @Rest\View(serializerGroups={"detail"})
     */
    public function delete(FolderRepository $repository, Folder $folder = null)
    {
        if (!$folder)
            throw new HttpException(Response::HTTP_NOT_FOUND, 'Folder with specified id not found');

        if (!$folder->getFiles()->isEmpty()) {;
            ErrorMessages::message(ErrorMessages::IS_NOT_EMPTY, "Folder ");
        }
        if ($repository->findBy(['parent' => $folder])){
            ErrorMessages::message(ErrorMessages::IS_NOT_EMPTY, "Folder ");
        }
        if ($folder->getMain() === $folder){
            $folder->setMain(null);
            $this->em->flush();
        }
        $this->em->remove($folder);
        $this->em->flush();
    }
}
