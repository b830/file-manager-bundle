<?php

namespace App\Akip\FileManagerBundle\Repository;

use App\Akip\FileManagerBundle\Entity\File;
use App\Akip\FileManagerBundle\Entity\Folder;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Folder|null find($id, $lockMode = null, $lockVersion = null)
 * @method Folder|null findOneBy(array $criteria, array $orderBy = null)
 * @method Folder[]    findAll()
 * @method Folder[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FolderRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Folder::class);
    }

    /**
     * Metodu je potreba zavolat po kazde uprave menu (pridani/uprava/smazani polozky, zmena razeni, zmena rodice apod.)
     * @see https://php.vrana.cz/traverzovani-kolem-stromu-prakticky.php
     * @see https://www.interval.cz/clanky/metody-ukladani-stromovych-dat-v-relacnich-databazich/
     *
     * @param $mainFolderId
     * @param null $parentId
     * @param int $left
     * @param int $depth
     * @return int|mixed
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function rebuildTree($mainFolderId, $parentId = null, $left = 0, $depth = 0)
    {
        $right = $left + 1;

        $items = $this->findBy(['main' => $mainFolderId, 'parent' => $parentId]);

        foreach ($items as $item) {
            $right = $this->rebuildTree($mainFolderId, $item->getId(), $right, $depth + 1);
        }

        if ($parentId) {
            $item = $this->find($parentId);
            $item->setLft($left);
            $item->setRgt($right);
            $item->setDepth($depth);

            $em = $this->getEntityManager();
            $em->persist($item);
            $em->flush();
        }

        return $right + 1;
    }

    public function getMaxDpth(Folder $main)
    {
        foreach ($this->findBy(['main' => $main]) as $item) {
            $depth_arr[] = $item->getDepth();
            $maxDpth = max($depth_arr);
        }
        /** @var int $maxDpth */
        if (!$maxDpth)
            return 1;
        return $maxDpth;
    }

    public function build(Folder $main,$em, $maxDpth, $data = null, $depth = 1)
    {
        if ($data === null) {
            if ($main->getParent() === null)
                $parent = null;
            else
                $parent = $main->getParent()->getId();
            $data = $this->setFields($main,$em, $parent);
        }
        $items = $this->findBy(['parent' => $data['id']], ['name' => 'ASC']);
        foreach ($items as $item) {
            $ar = $this->setFields($item,$em, $item->getParent()->getId());
            array_push($data['subfolders'], $ar);
        }
        if ($depth <= $maxDpth) {
            for ($i = 0; $i < count($data['subfolders']); $i++) {
                if ($data['subfolders'][$i]['parent'] === $data['id']) {
                    $data['subfolders'][$i] = $this->build($main,$em, $maxDpth, $data['subfolders'][$i], $depth + 1);
                } else
                    $data['subfolders'] = [];
            }
            return $data;
        }
    }

    public function setFields(Folder $item,$em, $parent = null)
    {
        $filesUuid = $this->createQueryBuilder('f')
            ->select(['file.uuid'])
            ->innerJoin('f.files', 'file')
            ->where('file.folder = :folder')
            ->andWhere('file.parentFile is NULL')
            ->setParameter('folder', $item)
            ->orderBy('LENGTH(file.originalName), file.originalName')->getQuery()->getResult();
        $files = [];
        foreach ($filesUuid as $uuid) {
            $file = $em->getRepository(File::class)->findOneBy(['uuid' => $uuid]);
            if (!$file->getChildren()->isEmpty() or (int)$file->getSize() === 0) {
                if ($file->getSize() !== '0')
                    $files[] = $file;
            }
         }
        return [
            'id' => $item->getId(),
            'name' => $item->getName(),
            'depth' => $item->getDepth(),
            'parent' => $parent,
            'files' => $files,
            'subfolders' => []
        ];
    }
}
